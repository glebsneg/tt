import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {OwnerEntity} from "../../interfaces/interfaces";
import {ICarOwnersService} from "../../services/i-car-owners.service";
import {ERRORS, LOGIN_TYPES} from '../../constants/constants';
import {Subscription} from "rxjs";
import {MyErrorStateMatcher, MyValidators} from "../../validators/my.validators";
import {RxwebValidators} from "@rxweb/reactive-form-validators";
import {ErrorStateMatcher} from "@angular/material/core";
import * as equal from 'fast-deep-equal';

@Component( {
  selector: 'app-owner-cru',
  templateUrl: './owner-cru.component.html',
} )

export class OwnerCruComponent implements OnInit, OnDestroy {
  createOwnersSubscription: Subscription | undefined;
  updateOwnerSubscription: Subscription | undefined;
  getOwnersSubscription: Subscription | undefined;
  getOwnerByIdSubscription: Subscription | undefined;
  routeQueryParamsSubscription: Subscription | undefined;
  routeParamsSubscription: Subscription | undefined;

  title: string = '';
  loginType: string = '';
  ownerId!: number;
  showForm: boolean = false;
  LOGIN_TYPES = LOGIN_TYPES;
  owner!: OwnerEntity;
  owners?: OwnerEntity[];
  errors: Object = ERRORS;
  form!: FormGroup;
  myErrorStateMatcher:ErrorStateMatcher = new MyErrorStateMatcher();
  submitted:boolean = false;
  isFormChanged:boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private iCarOwnersService: ICarOwnersService,
    private MyValidators: MyValidators
  ) {}

  ngOnInit(): void {
    this.routeParamsSubscription = this.route.params.subscribe( (params: Params) => {
      this.ownerId = +params.id;
    } );

    this.routeQueryParamsSubscription = this.route.queryParams.subscribe( (params: Params) => {
      this.loginType = params.loginType;
    } );

    if (!Number.isNaN( this.ownerId )) {
      this.getOwnerByIdSubscription = this.iCarOwnersService.getOwnerById( this.ownerId ).subscribe(
        (ownersEntity: OwnerEntity) => {
          this.owner = ownersEntity;

          this.formPatchValue();

          this.loginType === LOGIN_TYPES.read ? this.form.disable() : null;

          this.formOnChanges();

          this.showForm = true;
        }
      );
    } else {
      this.showForm = true;
    }

    switch (this.loginType) {
      case LOGIN_TYPES.create : {
        this.title = 'Новый владелец';
        break;
      }
      case LOGIN_TYPES.read : {
        this.title = 'Просмотр владельца';
        break;
      }
      case LOGIN_TYPES.update : {
        this.title = 'Обновление владельца';
        break;
      }
    }

    this.form = new FormGroup( {
      aFirstName: new FormControl( '', [
        Validators.required,
        Validators.pattern( '[a-zA-Zа-яА-Я ]*' )
      ] ),
      aLastName: new FormControl( '', [
        Validators.required,
        Validators.pattern( '[a-zA-Zа-яА-Я ]*' )
      ] ),
      aMiddleName: new FormControl( '', [
        Validators.required,
        Validators.pattern( '[a-zA-Zа-яА-Я ]*' )
      ] ),
      aCars: new FormArray( [this.getCarGroup()] ),
    } )

  }

  ngOnDestroy(): void {
    this.createOwnersSubscription?.unsubscribe();
    this.updateOwnerSubscription?.unsubscribe();
    this.updateOwnerSubscription?.unsubscribe();
    this.getOwnersSubscription?.unsubscribe();
    this.routeQueryParamsSubscription?.unsubscribe();
    this.routeParamsSubscription?.unsubscribe();
    this.getOwnerByIdSubscription?.unsubscribe();
  }

  getErrorMessage(formControl: any, nameOfFormControl: string): string {
    if (formControl.hasError( 'minlength' )) {
      return (this.errors as any)[nameOfFormControl].minLength
    }

    if (formControl.hasError( 'pattern' )) {
      return (this.errors as any)[nameOfFormControl].pattern
    }

    if (formControl.hasError( 'min' )) {
      return (this.errors as any)[nameOfFormControl].min
    }

    if (formControl.hasError( 'max' )) {
      return (this.errors as any)[nameOfFormControl].max
    }

    if (formControl.hasError( 'unique' )) {
      return (this.errors as any)[nameOfFormControl].unique
    }

    if (formControl.hasError( 'isDuplicateCarNumbers' )) {
      return (this.errors as any)[nameOfFormControl].isDuplicateCarNumbers;
    }

    if (formControl.hasError( 'required' )) {
      return (this.errors as any).required;
    }

    return '';
  }

  getCarGroup(): FormGroup {
    return new FormGroup( {
      number: new FormControl( '', {
          validators: [
            Validators.required,
            Validators.pattern( new RegExp( '[A-Z]{2}[\\d]{4}[A-Z]{2}' ) ),
            Validators.minLength( 8 ),
            Validators.maxLength( 8 ),
            RxwebValidators.unique(),
          ],
          asyncValidators: [this.MyValidators.isDuplicateCarNumbers( this.ownerId )]
        }
      ),
      brand: new FormControl( '', [
        Validators.required,
        Validators.pattern( '[a-zA-Zа-яА-Я\\d ]*' ),
      ] ),
      model: new FormControl( '', [
        Validators.required,
        Validators.pattern( '[a-zA-Zа-яА-Я\\d ]*' ),
      ] ),
      year: new FormControl( '', [
        Validators.required,
        Validators.min( 1990 ),
        Validators.max( (new Date).getFullYear() ),
        Validators.minLength( 4 ),
        Validators.maxLength( 4 ),
      ] )
    } )
  }

  addCarGroup(): void {
    (this.form.get( 'aCars' ) as FormArray).push( this.getCarGroup() );
  }

  removeCarGroup(i: number): void {
    (this.form.get( 'aCars' ) as FormArray).removeAt( i );
  }

  getFormArray(name: string): FormArray {
    return (this.form.get( name ) as FormArray);
  }

  formPatchValue(): void {
    for (let i = 0; i < this.owner.aCars.length - 1; i++) {
      this.addCarGroup();
    }

    this.form.patchValue( {
      ...this.owner
    } );
  }

  submit(): void {
    this.submitted = true;

    if (this.loginType === LOGIN_TYPES.create) {
      this.owner = this.form.value;

      this.createOwnersSubscription = this.iCarOwnersService.createOwner( this.owner )
        .subscribe( () => {
            this.router.navigate( ['/'] );
          }
        );
    }

    if (this.loginType === LOGIN_TYPES.update) {
      this.owner = {
        ...this.form.value,
        id: this.owner.id
      }

      this.updateOwnerSubscription = this.iCarOwnersService.editOwner( this.owner )
        .subscribe( () => {
          this.router.navigate( ['/'] );
        } );
    }

  }

  formOnChanges():void {
    this.form.valueChanges.subscribe((value:OwnerEntity) => {
      let owner:OwnerEntity = { ...this.owner };
      delete owner['id'];
      this.isFormChanged = !equal(owner, value);
    });
  }
}
