import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material/table';
import {OwnerEntity} from "../../interfaces/interfaces";
import {ICarOwnersService} from "../../services/i-car-owners.service";
import {Subscription} from "rxjs";
import {LOGIN_TYPES} from "../../constants/constants";

@Component( {
  selector: 'app-owners-list',
  templateUrl: './owners-list.component.html',
  styleUrls: ['./owners-list.component.scss']
} )
export class OwnersListComponent implements OnInit, OnDestroy {
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.mobile = window.screen.width <= 480;
  }

  LOGIN_TYPES = LOGIN_TYPES;

  title = "Владельцы автомобилей";
  owners: OwnerEntity[] = [];
  mobile: boolean = false;
  getOwnersSubscription: Subscription | undefined;
  deleteOwnerSubscription: Subscription | undefined;

  dataSource = new MatTableDataSource<OwnerEntity>( this.owners );
  displayedColumnsOfOwners: string[] = ['aFirstName', 'aLastName', 'aMiddleName', 'aCars'];
  selection = new SelectionModel<OwnerEntity>( false, [] );

  constructor(private iCarOwnersService: ICarOwnersService) {}

  ngOnInit(): void {
    this.getOwnersSubscription = this.iCarOwnersService.getOwners().subscribe(
      owners => this.owners = owners
    );

    this.mobile = window.screen.width <= 480;
  }

  ngOnDestroy(): void {
    this.getOwnersSubscription?.unsubscribe()
    this.deleteOwnerSubscription?.unsubscribe();
  }

  deleteOwner(): void {
    if (this.selection.selected[0].id !== undefined) {
      this.deleteOwnerSubscription = this.iCarOwnersService.deleteOwner(
        this.selection.selected[0].id ).subscribe(
        owners => this.owners = owners
      );
    }
  }

}
