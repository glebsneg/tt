import {Injectable} from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';
import {OwnerEntity} from "../interfaces/interfaces";

@Injectable( {
  providedIn: 'root',
} )

export class MockDataService implements InMemoryDbService {
  constructor() { }
  createDb() {
    return {
      ownersEntity : [
        {
          id:0, aLastName: 'Дали', aFirstName: 'Сальвадор', aMiddleName: 'Иванович',
          aCars: [
            {number: 'QW1231QW', brand: 'Bmw', model: 'x5', year: 1990},
            {number: 'ER2222RE', brand: 'Audi', model: 'Q7', year: 2018}
          ]
        },
        {
          id:1, aLastName: 'Snezhko', aFirstName: 'Gleb', aMiddleName: 'Svyatoslavovich',
          aCars: [{number: 'BN2141SD', brand: 'Volkswagen', model: 'Touareg', year: 2020}]
        },
        {
          id:2, aLastName: 'Эйнштейн', aFirstName: 'Альберт', aMiddleName: 'Иванович',
          aCars: [{number: 'BN2168OP', brand: 'Audi', model: 'Q7', year: 2000}]
        },
      ],
    }
  }
  genId(owners: OwnerEntity[]): number {
      // @ts-ignore
      return owners.length > 0 ? Math.max(...owners.map(owner => owner.id)) + 1 : 0;
  }
}
