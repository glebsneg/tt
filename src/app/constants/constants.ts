import {LoginTypes} from "../interfaces/interfaces";

export const LOGIN_TYPES: LoginTypes = {
  create: 'create',
  read: 'read',
  update: 'update',
}

export const ERRORS: Object = {
  required: "Поле должно быть заполнено",
  fullName: {
    pattern: "Должно содержать только буквы",
  },
  number: {
    pattern: "Неправильный формат",
    minLength: "Номер должен содержать 8 символов",
    isDuplicateCarNumbers: "Номер уже содержится в базе",
    unique: "Этот номер вы уже указали в форме",
  },
  brand: {
    pattern: "Допускаются только латинские символы",
  },
  model: {
    pattern: "Допускаются только латинские символы",
  },
  year: {
    minLength: "Год должен содержать 4 символа",
    pattern: "Допускаются только цифры и года в дипазоне 1990 – текущий год",
    min: "Минимальный год производства – 1990",
    max: "Максимальный год производства – текущий",
  },
}
