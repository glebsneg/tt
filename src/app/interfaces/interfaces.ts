import {Observable} from "rxjs";

export interface CarEntity {
  number: string,
  brand: string,
  model: string,
  year: number,
}

export interface OwnerEntity {
  id?: number,
  aLastName: string,
  aFirstName: string,
  aMiddleName: string,
  aCars: CarEntity[]
}

export interface ICarOwnersService {
  getOwners(): Observable<OwnerEntity[]>;
  getOwnerById(aId: number): Observable<OwnerEntity>;
  createOwner(ownerEntity: OwnerEntity): Observable<OwnerEntity>;
  editOwner(aOwner: OwnerEntity): Observable<OwnerEntity>;
  deleteOwner(aOwnerId: number): Observable<OwnerEntity[]>;
}

export interface LoginTypes {
  create: string,
  read: string,
  update: string,
}
