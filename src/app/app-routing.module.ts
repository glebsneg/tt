import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {OwnerCruComponent} from "./pages/owner-cru/owner-cru.component";
import {OwnersListComponent} from "./pages/owners-list/owners-list.component";

const routes: Routes = [
  {path: '', component: OwnersListComponent},
  {path: 'owner-cru', component: OwnerCruComponent},
  {path: 'owner-cru/:id', component: OwnerCruComponent},
]


@NgModule( {
  imports: [RouterModule.forRoot( routes )],
  exports: [RouterModule],
} )

export class AppRoutingModule {

}
