import {Injectable} from '@angular/core';
import {Observable, throwError} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import {OwnerEntity} from "../interfaces/interfaces";

@Injectable( {
  providedIn: 'root'
} )

export class ICarOwnersService implements ICarOwnersService {
  private url = 'api/ownersEntity';

  constructor(private http: HttpClient) {}

  getOwners(): Observable<OwnerEntity[]> {
    return this.http.get<OwnerEntity[]>( this.url ).pipe(
      catchError( (error: HttpErrorResponse) => {
        console.error( error );
        return throwError( error );
      } )
    );
  }

  getOwnerById(id: number): Observable<OwnerEntity> {
    return this.http.get<OwnerEntity>( `${this.url}/${id}` ).pipe(
      catchError( (error: HttpErrorResponse) => {
        console.error( error );
        return throwError( error );
      } )
    );
  }

  createOwner(owner: OwnerEntity): Observable<OwnerEntity> {
    owner.id = undefined;
    return this.http.post<OwnerEntity>( this.url, owner ).pipe(
      catchError( (error: HttpErrorResponse) => {
        console.error( error );
        return throwError( error );
      } )
    )
  }

  editOwner(aOwner: OwnerEntity): Observable<OwnerEntity> {
    return this.http.put<OwnerEntity>( `${this.url}/${aOwner.id}`, aOwner ).pipe(
      catchError( (error: HttpErrorResponse) => {
        console.error( error );
        return throwError( error );
      } )
    )
  }

  deleteOwner(aOwnerId: number): Observable<OwnerEntity[]> {
    this.http.delete<void>( `${this.url}/${aOwnerId}` ).subscribe();
    return this.getOwners();
  }

}
