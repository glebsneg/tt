import {
  AbstractControl,
  AsyncValidatorFn,
  FormControl,
  FormGroupDirective,
  NgForm,
  ValidationErrors
} from "@angular/forms";
import {Observable} from "rxjs";
import {ICarOwnersService} from "../services/i-car-owners.service";
import {OwnerEntity} from "../interfaces/interfaces";
import {Injectable} from "@angular/core";
import {map} from "rxjs/operators";
import {ErrorStateMatcher} from "@angular/material/core";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && control.dirty);
  }
}

@Injectable( {
  providedIn: 'root',
} )
export class MyValidators {
  constructor(private iCarOwnersService: ICarOwnersService) {
  }

  ownerId!: number

  private isCarNumbersExist(number: string): Observable<boolean> {
    return this.iCarOwnersService.getOwners().pipe(
      map( (owners: OwnerEntity[]) => {
        let answer: boolean = false;

        owners.forEach( (owner: OwnerEntity) => {
            if (owner.id !== this.ownerId) {
              if (owner.aCars.some( aCar =>
                aCar.number === number
              )) {
                answer = true;
              }
            }
          }
        )
        return answer;
      } )
    );
  }

  isDuplicateCarNumbers(ownerId: number): AsyncValidatorFn {
    this.ownerId = ownerId;

    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return this.isCarNumbersExist( control.value ).pipe(
        map( (response: boolean) =>
          response ? {isDuplicateCarNumbers: true} : null )
      )
    }
  }

}


